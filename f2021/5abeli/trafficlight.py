from machine import Pin
from time import sleep

ledRed = Pin(15, Pin.OUT)
ledYellow = Pin(14, Pin.OUT)
ledGreen = Pin(13, Pin.OUT)
iled = Pin(25, Pin.OUT)
ledALL = [ledRed, ledYellow, ledGreen]
while True:
    ledRed.on()
    iled.on()
    sleep(1)
    for _ in range(4):
        iled.off()
        sleep(0.5)
        iled.on()
        sleep(0.5)
    iled.off()
    ledYellow.on()
    sleep(2)
    for led in ledALL:
        led.off()
    ledGreen.on()
    sleep(3)
    for _ in range (4):

        ledGreen.off()
        sleep(0.5)
        ledGreen.on()
        sleep(0.5)
    ledGreen.off()
    ledYellow.on()
    sleep(2)
    ledYellow.off()
    ledRed.on()
