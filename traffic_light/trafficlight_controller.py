"""
Traffic light controller simulating an Austrian traffic light.
"""

from time import sleep

class TrafficLightController:
    """
    Traffic light controller simulating an Austrian traffic light.

    When starting, the red light is on.
    red -> red + yellow -> green -> green blinks 4 times -> yellow -> RESTART
    """
    BLINK_DURATION = 0.5
    SHORT_DURATION= 2
    LONG_DURATION = 5

    def __init__(self, green, yellow, red):
        """
        Initialize Controller with three lights.

        Each light must have an API offering `on()` and `off()`.
        """
        self.green = green
        self.yellow = yellow
        self.red = red

    def start(self):
        """
        Start an infinite loop through the controller's states.
        """
        while True:
            self.state_red()
            self.state_yellow_red()
            self.state_green()
            self.state_blinking()
            self.state_yellow()

    def state_red(self):
        self.green.off()
        self.yellow.off()
        self.red.on()
        sleep(self.LONG_DURATION)

    def state_yellow_red(self):
        self.green.off()
        self.red.on()
        self.yellow.on()
        sleep(self.SHORT_DURATION)

    def state_green(self):
        self.green.on()
        self.yellow.off()
        self.red.off()
        sleep(self.LONG_DURATION)

    def state_blinking(self):
        self.green.on()
        self.yellow.off()
        self.red.off()
        for _ in range(8):
            self.green.toggle()
            sleep(self.BLINK_DURATION)

    def state_yellow(self):
        self.green.off()
        self.yellow.on()
        self.red.off()
        sleep(self.SHORT_DURATION)
