# Micropython Examples
The example projects in this repository are created for or by students
in order to learn micropython on a Raspberry Pi Pico.
## Getting started
Install an editor that works well with programming the Pico. I suggest
either [Thonnny](https://thonny.org/)

    pip install --user -U thonny

or VSCode / [VSCodium](https://vscodium.com/) with the
[Pico-Go extension](http://pico-go.net/).

Make sure that your user is allowed to communicate with your Pico by adding it
to the `dialout` group

    sudo adduser $USER dialout

Alternatively, programs can be copied to an from a micropython device with
tools like `ampy` or `rshell`. These can be installed directly via `pip`.

    pip install --user rshell
    pip install --user adafruit-ampy

